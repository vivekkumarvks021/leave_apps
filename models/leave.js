var mongoose = require('mongoose');
const Schema = mongoose.Schema;

var leave = new Schema({
                employeeName: {
                    type: String,
                    required: true
                },
                employeeEmail: {
                    type: String,
                    required: true
                },
                dateOfLeave: [Date],
                reasonOfLeave: { 
                    type: String,
                    required: true
                },
                manager:{
                    name: String,
                    email: String,
                }
});

module.exports = mongoose.model('leave', leave);
