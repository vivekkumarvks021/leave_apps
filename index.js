const express=require('express');
const app=new express();

const router = require('./router/router');
var bodyParser = require("body-parser");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/leave_app', ()=>{
    console.log("Database connected")
})

app.use('/',router);
app.listen(3000,()=>{
    console.log('server running on 3000')
})
