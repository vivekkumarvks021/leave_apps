const DbLeaveData = require('../models/leave');
const nodeMailer = require('nodemailer');
const emailRegex= /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    async function register(req, res){
        let checkEmpEmail,checkManagerEmail;
    if(!req.body.employeeEmail || !req.body.managerEmail){
        res.json({
            success:false,
            msg: "employee email is required"
        })
    }else{
        checkEmpEmail = emailRegex.test(req.body.employeeEmail);
        checkManagerEmail = emailRegex.test(req.body.managerEmail);
    }

    if(!checkEmpEmail || !checkManagerEmail){
        res.json({
            success: false,
            msg: "Invalid email"
        })
    }else if(checkEmpEmail && checkManagerEmail){
        try {
            let data  = new DbLeaveData({
                employeeName: req.body.employeeName,
                employeeEmail: req.body.employeeEmail,
                dateOfLeave: req.body.date,
                reasonOfLeave: req.body.reasonOfLeave,
                manager:{
                    name: req.body.managerName,
                    email: req.body.managerEmail
                }
            });

            let data_save = await data.save()
            console.log('saved data', data_save);

            let content = `<h1>Dear ${req.body.managerName}</h1>
                            ${req.body.employeeName} send the application for leave of ${req.body.date.length} days,
                            the reason for leave is following:- 
                            ${req.body.reasonOfLeave} 
                            for date ${req.body.date}`;

            let message = {
                text: "",
                from: "Shippigo <raahul410@gmail.com>",
                to: 'vivek.kumar@zenways.io',
                subject: 'Leave Application',
                html: content
            };

            if(data_save){
                let transporter = nodeMailer.createTransport({
                    service: 'Gmail',
                    auth: {
                        user: 'xyz@gmail.com',
                        pass: 'xxx'
                    }
                });
                
                await transporter.sendMail(message);
            }
            res.json({
                success: true,
                msg: "Application submitted successfully"
            })

        }catch(error){
            res.json({
                success:false,
                msg:"something went wrong."
            })
        }

    }    
    } 
module.exports.register = register;