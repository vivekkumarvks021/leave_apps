let express = require('express')
let router =express.Router();

let registerLeave = require('./registerLeave');
router.post('/registerLeave',registerLeave.register)

let manageLeave = require('./manageLeave');
router.post('/manageLeave',manageLeave.manage)

let leaveData = require('./leaveData');
router.post('/leaveData',leaveData.leaveData)

module.exports = router;