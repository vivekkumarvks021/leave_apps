const DbLeaveData = require('../models/leave');
const json2csv = require('json2csv').Parser;


module.exports.leaveData = async(req,res)=>{
    let condition = {
        $and:[
            {'dateOfLeave':{$gte:req.body.startDate}},
            {'dateOfLeave':{$lte:req.body.endDate}}
        ]
    }

    try {
        DbLeaveData.find(condition,async(error,data)=>{
            
            let headers = ["S.No.", "data"]

            const json2csvParser = await new json2csv({ headers });
            let csvdata=[];
            data.map(ele=>{
               let userData = {
                    employeeName:ele.employeeName,
                    employeeEmail:ele.employeeEmail,
                    reasonOfLeave:ele.reasonOfLeave,
                    dateOfLeave:ele.dateOfLeave,
                    managerName:ele.manager.name,
                    managerEmail:ele.manager.email
                }
                csvdata.push(userData);
            })

            let csv = await json2csvParser.parse(csvdata);
                res.setHeader('Content-disposition', 'attachment; filename=data.csv');
                res.set('Content-Type', 'text/csv');
                res.status(200).send(csv);
        })
            
            
            

        }catch(error){

            console.log(error);

        }
}
