const DbLeaveData = require('../models/leave');


module.exports.manage = async(req,res)=>{
    try {

        let condition = {
            $and:[
                {'dateOfLeave':{$gte:req.body.date}},
                //{'dateOfLeave':{$lte:req.body.date}},
                {'manager.email':req.body.managerEmail}
            ]
        }

        DbLeaveData.find(condition,async(error,data)=>{
                res.json({
                    success: true,
                    msg:"leave application data",
                    data:data
                })
        })
            
    }catch(error){
            res.json({
                success:false,
                msg:"someting went wrong"
            })
    }
}
